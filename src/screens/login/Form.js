import React, { Component } from "react";
import {
  Text,
  View,
  TextInput,
  TouchableOpacity
} from 'react-native';

import styles from "./styles";

class Form extends Component {
  render() {
    return (
      <View style={styles.formContainer}>
        <TextInput
          style={styles.textInput}
          placeholder='Email'
          placeholderTextColor='white'
        />
        <TextInput
          style={styles.textInput}
          placeholder='password'
          secureTextEntry={true}
          placeholderTextColor='rgba(255,255,255, 0.7)'
        />
        <TouchableOpacity
          style={styles.button}
          onPress={this.props.goPage}
        >
          <Text
            style={styles.buttonText}>
            {this.props.type}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default Form;
