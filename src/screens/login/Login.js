import React, { Component } from "react";
import {
  StatusBar,
  Text,
  View
} from 'react-native';
import { Container } from "native-base";

import styles from "./styles";
import Logo from './Logo';
import Form from './Form';


class Login extends Component {
  goPage = () => {
    this.props.navigation.navigate("Home");
  }

  render() {
    return (
      <Container style={styles.container}>
        <StatusBar backgroundColor="#37474F" barStyle="light-content" />
        <Logo />
        <Form type="Login"
          goPage={this.goPage} />
        <View style={styles.signupTextCont}>
          <Text style={styles.signupText}>
            Don't have an account yet?
          </Text>
          <Text
            style={styles.signupButton}
            onPress={() => this.props.navigation.navigate("Signup")}
          >
            Signup
          </Text>
        </View>
      </Container>
    );
  }
}

export default Login;
