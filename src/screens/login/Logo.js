import React, { Component } from "react";
import { Image, View } from "react-native";
import { Text } from "native-base";

import styles from "./styles";

const logo = require("../../../assets/logo.png");

class Logo extends Component {
  render() {
    return (
      <View style={styles.logoContainer}>
        <Image source={logo}
          style={styles.imageContainer}
        />
        <Text style={styles.logoText}>
          Welcome to My app.
        </Text>
      </View>
    );
  }
}

export default Logo;
