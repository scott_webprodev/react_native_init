const React = require("react-native");
const { Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;

export default {
  container: {
    flex: 1,
    backgroundColor: '#455A64',
    alignItems: "center",
    justifyContent: "center"
  },
  logoContainer: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "center",
  },
  formContainer: {
    flexGrow: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  imageContainer: {
    width: 100,
    height: 100
  },
  logoText: {
    marginVertical: 20,
    fontSize: 18,
    color: 'rgba(255, 255, 0, 0.7)'
  },
  textInput: {
    width: 300,
    backgroundColor: 'rgba(255,255,255, 0.3)',
    borderRadius: 25,
    marginVertical: 10,
    paddingHorizontal: 16,
    color: 'white'
  },
  button: {
    backgroundColor: "#37474F",
    width: 300,
    borderRadius: 25,
    marginVertical: 10,
    paddingVertical: 13,
  },
  buttonText: {
    fontSize: 16,
    color: 'white',
    textAlign: 'center'
  },
  signupTextCont: {
    // flexGrow: 1,
    alignItems: "center",
    justifyContent: "flex-end",
    marginVertical: 16,
    flexDirection: 'row'
  },
  signupText: {
    color: 'rgba(255,255,255,0.7)',
    fontSize: 16
  },
  signupButton: {
    color: 'white',
    fontSize: 16,
    fontWeight: '500'
  }
};
