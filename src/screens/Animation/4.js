import React from 'react';
import {
  NativeModules,
  LayoutAnimation,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {
  Container,
  Header,
  Title,
  Button,
  Left,
  Icon,
  Body,
} from "native-base";
import styles from "./styles";

const { UIManager } = NativeModules;

UIManager.setLayoutAnimationEnabledExperimental &&
  UIManager.setLayoutAnimationEnabledExperimental(true);

export default class App extends React.Component {
  state = {
    w: 100,
    h: 100,
  };

  _onPress = () => {
    // Animate the update
    LayoutAnimation.spring();
    this.setState({ w: this.state.w + 15, h: this.state.h + 15 })
  }

  render() {
    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>Scroll images</Title>
          </Body>
        </Header>
        <View style={styles.subContainer}>
          <View style={[styles.redBox, { width: this.state.w, height: this.state.h }]} />
          <TouchableOpacity onPress={this._onPress}>
            <View style={styles.button}>
              <Text style={styles.buttonText}>Press me!</Text>
            </View>
          </TouchableOpacity>
        </View>
      </Container>
    );
  }
}