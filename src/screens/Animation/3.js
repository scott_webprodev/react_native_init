import React, { useRef, Component } from "react";
import { Animated, View, PanResponder, Text } from "react-native";
import {
  Container,
  Header,
  Title,
  Button,
  Left,
  Icon,
  Body,
} from "native-base";
import styles from "./styles";

const DragBox = (props) => {
  const pan = useRef(new Animated.ValueXY()).current;
  const panResponder = useRef(
    PanResponder.create({
      onMoveShouldSetPanResponder: () => true,
      onPanResponderMove: Animated.event([
        null,
        { dx: pan.x, dy: pan.y }
      ]),
      onPanResponderRelease: () => {
        Animated.spring(pan, { toValue: { x: 0, y: 0 } }).start();
      }
    })
  ).current;

  return (
    <View style={styles.subContainer}>
      <Text style={styles.titleText}>Drag & Release this box!</Text>
      <Animated.View
        style={{
          transform: [{ translateX: pan.x }, { translateY: pan.y }]
        }}
        {...panResponder.panHandlers}
      >
        <View style={styles.blueBox} />
      </Animated.View>
    </View>
  );
}

class Animation3 extends Component {
  render() {
    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>Drag & Release</Title>
          </Body>
        </Header>
        <DragBox />
      </Container>
    );
  }
}

export default Animation3;

