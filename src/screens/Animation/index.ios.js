import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Right,
  Body,
  Text,
  ListItem,
  List
} from "native-base";
import styles from "./styles";

const datas = [
  {
    route: "Animation1",
    text: "Fade In View"
  },
  {
    route: "Animation2",
    text: "Scroll images"
  },
  {
    route: "Animation3",
    text: "Drag & Release"
  },
  {
    route: "Animation4",
    text: "Grow Scale"
  },
  // {
  //   route: "Animation5",
  //   text: "Icon and Text Button"
  // },
  // {
  //   route: "Animation6",
  //   text: "Multiple Icon Buttons"
  // },
  // {
  //   route: "Animation7",
  //   text: "Title and Subtitle"
  // },
  // {
  //   route: "Animation8",
  //   text: "Custom Background Color"
  // }
];

class AnimationNB extends Component {
  render() {
    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.openDrawer()}
            >
              <Icon name="menu" />
            </Button>
          </Left>
          <Body>
            <Title>Animations</Title>
          </Body>
          <Right />
        </Header>

        <Content>
          <List
            dataArray={datas}
            renderRow={data =>
              <ListItem
                button
                onPress={() => this.props.navigation.navigate(data.route)}
              >
                <Left>
                  <Text>
                    {data.text}
                  </Text>
                </Left>
                <Right>
                  <Icon name="arrow-forward" />
                </Right>
              </ListItem>}
          />
        </Content>
      </Container>
    );
  }
}

export default AnimationNB;
